﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TreasureGame
{
    public enum ChestType
    {
        Closed_PirateChest_Empty = 100,
        Closed_PirateChest_Gold = 110,
        Closed_PirateChest_Silver = 120,
        Closed_PirateChest_Doubloon = 130,
        Closed_PirateChest_Copper = 140,
        Open_PirateChest_Empty = 150,
        Open_PirateChest_Gold = 160,
        Open_PirateChest_Silver = 170,
		Open_PirateChest_Doubloon = 180,
		Open_PirateChest_Copper = 190,
    };
}
