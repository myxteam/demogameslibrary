﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreasureGame
{
    public class TreasureGame
    {

        public Guid GUID { get; set; }

        // Id of this game session, different value for each game
        public int Session { get; set; }

        // the player of the game
        public Player Player { get; set; }

        // the setp of the game (number of rows/collums of chests)
        public Grid Grid { get; set; }

        public TreasureGame(int id, int collums, int rows, string setup)
        {
            this.Session = id;
            this.Grid = new Grid(collums, rows, setup);

        }
    }
}
