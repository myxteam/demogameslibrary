﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TreasureGame
{
	public class Grid
	{
		// number of rows of chests
		public int Rows { get; set; }

		// number of collums of chests
		public int Collums { get; set; }

		// internal chest array, first array = rows, seconds array = collums
		public Chest[,] Chests;

		public Chest getChest (int row, int collum)
		{
			// TO DO
			return Chests [row, collum];
		}

		public void setChest (int x, int y, Chest chest)
		{
			this.Chests [x, y] = chest;

		}

		public Grid (int cols, int rows, String setup)
		{
			this.Rows = rows;
			this.Collums = cols;

			this.Chests = new Chest[Rows, Collums];

			ChestType ctype;
			var y2 = setup.ToString ().Split (':');
			int i = 2;
			for (int x = 0; x < Rows; x++) {
				for (int y = 0; y < Collums; y++) {
					i++;
					setChest (x, y, new Chest ());
					Chests [x, y].CurrencyName = y2 [i].Split (',') [0];
					switch (Chests [x, y].CurrencyName) {
					case "NULL":
						{
							Chests [x, y].ChestType = ChestType.Closed_PirateChest_Empty;
							Chests [x, y].FK_CURRENCY_ID = 0;
							Chests [x, y].Ammount = int.Parse (y2 [i].Split (',') [1]);
							break;
						}
					case TreasureGameModels.GameCurrenciesConstant.GOLD:
						{
							Chests [x, y].ChestType = ChestType.Closed_PirateChest_Gold;
							Chests [x, y].FK_CURRENCY_ID = 1;
							Chests [x, y].Ammount = int.Parse (y2 [i].Split (',') [1]);
							break;
						}
					case TreasureGameModels.GameCurrenciesConstant.SILVER:
						{
							Chests [x, y].ChestType = ChestType.Closed_PirateChest_Silver;
							Chests [x, y].FK_CURRENCY_ID = 2;
							Chests [x, y].Ammount = int.Parse (y2 [i].Split (',') [1]);
							break;
						}
					case TreasureGameModels.GameCurrenciesConstant.COPPER:
						{
							Chests [x, y].ChestType = ChestType.Closed_PirateChest_Copper;
							Chests [x, y].FK_CURRENCY_ID = 3;
							Chests [x, y].Ammount = int.Parse (y2 [i].Split (',') [1]);
							break;
						}
					case TreasureGameModels.GameCurrenciesConstant.DOUBLOON:
						{
							Chests [x, y].ChestType = ChestType.Closed_PirateChest_Doubloon;
							Chests [x, y].FK_CURRENCY_ID = 4;
							Chests [x, y].Ammount = int.Parse (y2 [i].Split (',') [1]);
							break;
						}
					}
				}
			}

		}
	}
}

