﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TreasureGame
{
    public class Chest
    {
        // type of chest
       public ChestType ChestType {get; set;}
       public int FK_CURRENCY_ID;
       public string CurrencyName;
       public int Ammount;

       bool _isOpen = false;
       public bool isOpen {
           get
           {
               return _isOpen;
           }
           set
           {
               _isOpen = value;
           }


       }
    }
}
