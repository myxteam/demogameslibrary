//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//
//using Android.App;
//using Android.Content;
//using Android.OS;
//using Android.Runtime;
//using Android.Views;
//using Android.Widget;
//using Newtonsoft.Json;
////using PerpetualEngine.Storage;
//using RestSharp;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Net;
////using System.Net.Http;
//using System.Threading.Tasks;
//using System.IO;
//
//
//namespace TreasureHuntLib.API
//{
//
//    public class APICalls
//    {
//        public static String baseUrl = "http://glopho-dev.cloudapp.net/";
//        public enum Method { GET, POST, PUT, DELETE };
//        //------------------------------------------------------------------------------------------
//        //Create                                   Call Glopho Web api
//        //------------------------------------------------------------------------------------------
//        //objectToUpdate : object                  Object to send, null for void
//        //apiEndPoint    : string                  Url of the apicall e.g "api/AccountApi/JsonLogin"
//        //postOrGet: APICalls.Method postOrGet     Post or Get
//        //timeout : int                            if set, timeout for the request in ms
//        //overrideBaseUrl : string                 if set, override the baseurl                     
//        //-------------------------------------------------------------------------------------------
//        public static IRestResponse Create<T>(object objectToUpdate, string apiEndPoint, APICalls.Method postOrGet,
//            int timeout = -1, string overrideBaseUrl = null, bool useCookie = true) where T : new()
//        {
//            String url2Use = baseUrl;
//            RestSharp.Method _method = RestSharp.Method.GET;
//            switch (postOrGet)
//            {
//                case Method.GET:
//                    {
//                        _method = RestSharp.Method.GET;
//                        break;
//                    }
//                case Method.POST:
//                    {
//                        _method = RestSharp.Method.POST;
//                        break;
//                    }
//                case Method.PUT:
//                    {
//                        _method = RestSharp.Method.PUT;
//                        break;
//                    }
//                case Method.DELETE:
//                    {
//                        _method = RestSharp.Method.DELETE;
//                        break;
//                    }
//            }
//            url2Use = (overrideBaseUrl == null) ? baseUrl : overrideBaseUrl;
//
//            var client = new RestClient(url2Use)
//            {
//
//            };
//            client.AddDefaultHeader("X-Requested-With", "XMLHttpRequest");
//
//            var json = JsonConvert.SerializeObject(objectToUpdate);
//            var request = new RestRequest(apiEndPoint, _method);
//            //if (useCookie && SessionCookie != null)
//            //{
//            //    request.AddCookie(".ASPXAUTH", SessionCookie.Value);
//            //}
//            //else
//            //{
//            //}
//            if (timeout > 0)
//            {
//                request.Timeout = timeout;
//            }
//            else
//            {
//                request.Timeout = 25000;
//            }
//            request.AddParameter("text/json", json, ParameterType.RequestBody);
//            var response = client.Execute<T>(request);
//            return response;
//        }
//
//        public static void MessageHandler_in(bool showMessage)  // ...
//        // Maybe need reference to the ui component 
//        {
//            if (showMessage)
//            {
//                // show the Contacting server message on teh activity...
//            }
//        }
//
//        public static bool MessageHandler_Out(bool showMessage, ApiResult result)  // ...
//        // Maybe need reference to the ui component 
//        {
//            if (showMessage)
//            {
//
//
//                if (result.Status == ApiResponse.NETWORK_ERROR)
//                {
//                    // show the error message
//                    // return true if user clicks retry == yes"
//                    return true;
//                  
//                }
//                else if (result.Status == ApiResponse.OTHER_ERROR)
//                {
//                    // show the error message
//                    // return true if user clicks retry == yes"
//                    return true;
//                }
//                else if (result.Status == ApiResponse.OK)
//                {
//                    // close the message window
//                    return false; // dont have to retry
//                }
//            }
//            return false;
//        }
//
//        public static void ErrorHandler(bool showMessage, Exception e)
//        {
//            if (showMessage)
//            {
//
//                // show message (same as NETWORK_ERROR)
//                // handle user yes/no
//            }
//        }
//
//        public static ApiResult Login(LogInModel model, bool showMessage)  // if showMessage = false, dont show the message
//        // do we need to make it async? 
//        // Maybe need reference to the ui component 
//        {
//            try
//            {
//                MessageHandler_in(showMessage); // make it a function call so we dont have the same code in all APiCAlls
//
//                var x = APICalls.Create<ApiResult>(model, "api/Signup/GuidSignup", APICalls.Method.POST, -1, null, true);
//                ApiResult result = JsonConvert.DeserializeObject<ApiResult>(x.Content);
//
//                if (MessageHandler_Out(showMessage, result))
//                {
//                    return Login(model, showMessage);
//                }
//                return result;
//
//            }
//            catch (TimeoutException e)
//            {
//                ErrorHandler(showMessage, e);
//                return null;
//            }
//            catch (Exception e)
//            {
//                return null;
//            }
//        }
//
//
//        //public static ApiResult Signup_Guid(Guid guid, bool showMessage)
//        //{
//        //    try
//        //    {
//        //        MessageHandler_in(showMessage);
//
//        //        LogInModel model = new LogInModel();
//        //        model.GameID = Guid.NewGuid();
//        //        model.Password = "PASS";
//        //        model.UserName = "USER";
//        //        var x = APICalls.Create<ApiResult>(model, "api/Signup/GuidSignup", APICalls.Method.POST, -1, null, true);
//        //        ApiResult result = JsonConvert.DeserializeObject<ApiResult>(x.Content);
//
//        //        if (MessageHandler_Out(showMessage, result))
//        //        {
//        //            return Login(model, showMessage);
//        //        }
//        //        return result;
//        //    }
//        //    catch (TimeoutException e)
//        //    {
//        //        ErrorHandler(showMessage, e);
//        //        return null;
//        //    }
//        //    catch (Exception e)
//        //    {
//        //        return null;
//        //    }
//        //}
//
//
////        public static ApiResult Signup_Guid(Guid guid, bool showMessage)
////        {
////            try
////            {
////                MessageHandler_in(showMessage);
////
////                LogInModel model = new LogInModel();
////                model.GameID = Guid.NewGuid();
////                model.GUID = guid;
////                model.Password = "PASS";
////                model.UserName = "USER";
////                var x = APICalls.Create<ApiResult>(model, "api/Signup/GuidSignup", APICalls.Method.POST, -1, null, true);
////                ApiResult result = JsonConvert.DeserializeObject<ApiResult>(x.Content);
////
////                if (MessageHandler_Out(showMessage, result))
////                {
////                    return Login(model, showMessage);
////                }
////                return result;
////
////            }
////            catch (TimeoutException e)
////            {
////                ErrorHandler(showMessage, e);
////                return null;
////            }
////            catch (Exception e)
////            {
////                return null;
////            }
////        }
//    }
//}